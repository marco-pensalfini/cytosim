// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// StretchyFiber class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This is based on GrowingFiber and leverages the force applied at the fiber ends to
   drive elongation according to a prescribed constitutive behavior.
*/

#include "dim.h"
#include "assert_macro.h"
#include "stretchy_fiber.h"
#include "stretchy_fiber_prop.h"
#include "exceptions.h"
#include "iowrapper.h"
#include "simul.h"
#include "space.h"


//------------------------------------------------------------------------------

StretchyFiber::StretchyFiber(StretchyFiberProp const* p) : Fiber(p), prop(p)
{
    mStateM = STATE_GREEN;
    mStateP = STATE_GREEN;
    mGrowthM = 0;
    mGrowthP = 0;
}


StretchyFiber::~StretchyFiber()
{
    prop = nullptr;
}


//------------------------------------------------------------------------------
#pragma mark -


void StretchyFiber::setDynamicStateM(state_t s)
{
    if ( s == STATE_WHITE || s == STATE_GREEN )
        mStateM = s;
    else
        throw InvalidParameter("invalid AssemblyState for a StretchyFiber");
}


void StretchyFiber::setDynamicStateP(state_t s)
{
    if ( s == STATE_WHITE || s == STATE_GREEN )
        mStateP = s;
    else
        throw InvalidParameter("invalid AssemblyState for a StretchyFiber");
}

//------------------------------------------------------------------------------

void StretchyFiber::step()
{
    constexpr int P = 0, M = 1;

    /// simulation time and time step
    real tcurr       = age();               //current fiber age ( = simul().time() - birthTime() )
    real dt          = simul().time_step(); //simulation time step
    
    /// fiber geometry
    real len         = length();            //current fiber length
    real len0        = length_birth();      //fiber length at birth
    real strain_curr = len/len0 - 1.;       //current fiber strain

    /// fiber constitutive parameters
    real E1Af        = prop->constitutive[0];
    real Tau1        = prop->constitutive[1];
    real E2Af        = prop->constitutive[2];
    real Tau2        = prop->constitutive[3];

    /// fiber parameters for nonlinear response with softening & restiffening
    real E1Af_soften = prop->nonlinear[0];
    real E1Af_stiffen= prop->nonlinear[1];
    real eps_soften  = prop->nonlinear[2];
    real eps_stiffen = prop->nonlinear[3];
    real gamma       = prop->nonlinear[4];

    /// ensure linear fiber response if `nonlinear` parameters are not passed
    if ( E1Af_soften == INFINITY && E1Af_stiffen == INFINITY)
        {
            E1Af_soften  = E1Af;
            E1Af_stiffen = E1Af;
        }

    /// compute E1*Af ratios in flattening and restiffening part of fiber response
    real alpha_soften  = E1Af_soften/E1Af;
    real alpha_stiffen = E1Af_stiffen/E1Af;

    /// store current force values at each end
    forceCurrentP    = projectedForceEndP();
    forceCurrentM    = projectedForceEndM();

    /// measure force variations at each end ( = 0 for first step since fiber birth )
    if ( tcurr == dt )
    {
        deltaForceP.push_back(0);
        deltaForceM.push_back(0);
    }
    else
    {
        deltaForceP.push_back(forceCurrentP - forcePreviousP);
        deltaForceM.push_back(forceCurrentM - forcePreviousM);
    }

    /// current value of E1*Af
    E1Afvals.push_back( E1Af * ( 1 + (alpha_soften-1)/(1 + exp(-gamma*(strain_curr-eps_soften))) + (alpha_stiffen-alpha_soften)/(1 + exp(-gamma*(strain_curr-eps_stiffen))) ) );

    /// creep compliance function through superposition principle
    size_t num_tinc = deltaForceP.size();
    std::vector<real> Jfun;
    real Jfun_i;
    for (size_t i=0; i < num_tinc; ++i )
    {   
        Jfun_i = 0;
        // 1st Kelvin-Voigt model
        if ( prop->constitutive[0] < INFINITY && prop->constitutive[1] < INFINITY )
        {
            Jfun_i += ( 1.0 - exp( - (tcurr - i*dt) / Tau1 ) ) / E1Afvals[i];
            // 2nd Kelvin-Voigt model
            if ( prop->constitutive[2] < INFINITY && prop->constitutive[3] < INFINITY )
                Jfun_i += ( 1.0 - exp( - (tcurr - i*dt) / Tau2 ) ) / E2Af;
        }
        Jfun.push_back( Jfun_i );
    }

    if ( mStateP == STATE_GREEN )
    {
        mGrowthP = 0;
        real strain_tot = 0.0;
        /// determine total strain in response to loading history
        for (size_t i=0; i < num_tinc; ++i )
            strain_tot += Jfun[i]*deltaForceP[i];
        /// fiber elongation to be applied during current step to match strain_tot
        /// ( x0.5 because this is only one of two fiber ends )
        mGrowthP = 0.5 * ( strain_tot - strain_curr ) * len0;
    }
    else
    {
        mGrowthP = 0;
    }
    
    if ( mStateM == STATE_GREEN )
    {
        mGrowthM = 0;
        real strain_tot = 0.0;
        /// determine total strain in response to loading history
        for (size_t i=0; i < num_tinc; ++i )
            strain_tot += Jfun[i]*deltaForceM[i];
        /// fiber elongation to be applied during current step to match strain_tot
        /// ( x0.5 because this is only one of two fiber ends )
        mGrowthM = 0.5 * ( strain_tot - strain_curr ) * len0;

    }
    else
    {
        mGrowthM = 0;
    }
    
    /// apply elongation and check if fiber is too short/too long
    real inc = mGrowthP + mGrowthM;
    if ( len + inc < prop->min_length )
    {
        if ( !prop->persistent )
        {
            /// the fiber is too short, we delete it:
            delete(this);
            return;
        }
    }
    else if ( len + inc < prop->max_length )
    {
        if ( mGrowthM ) growM(mGrowthM);
        if ( mGrowthP ) growP(mGrowthP);
    }
    else if ( len < prop->max_length )
    {
        /// the remaining possible growth is distributed to the two ends:
        inc = ( prop->max_length - len ) / inc;
        if ( mGrowthM ) growM(inc*mGrowthM);
        if ( mGrowthP ) growP(inc*mGrowthP);
    }
    else
    {
        mGrowthM = 0;
        mGrowthP = 0;
    }

    Fiber::step();

    /// store current force value as 'previous' for next time step
    forcePreviousP = setPrevForceP(forceCurrentP);
    forcePreviousM = setPrevForceM(forceCurrentM);

}


//------------------------------------------------------------------------------
#pragma mark -


void StretchyFiber::write(Outputter& out) const
{
    Fiber::write(out);

    /// write variables describing the dynamic state of the ends:
    writeHeader(out, TAG_DYNAMIC);
    out.writeFloat(mGrowthM);
    out.writeFloat(mGrowthP);
}


void StretchyFiber::read(Inputter& in, Simul& sim, ObjectTag tag)
{

#ifdef BACKWARD_COMPATIBILITY
    if ( tag == TAG_DYNAMIC || ( tag == TAG && in.formatID() < 44 ) )
#else
    if ( tag == TAG_DYNAMIC )
#endif
    {
        mGrowthM = in.readFloat();
#ifdef BACKWARD_COMPATIBILITY
        if ( in.formatID() > 45 )
#endif
        mGrowthP = in.readFloat();
    }
#ifdef BACKWARD_COMPATIBILITY
    if ( tag != TAG_DYNAMIC || in.formatID() < 44 )
#else
    else
#endif
    {
#ifdef BACKWARD_COMPATIBILITY
        const real len = length();
#endif
        
        Fiber::read(in, sim, tag);
        
#ifdef BACKWARD_COMPATIBILITY
        if ( tag == TAG && in.formatID() < 46 )
        {
            // adjust growing variable
            mGrowthP = length() - len;
            mGrowthM = 0;
        }
#endif
    }
}
