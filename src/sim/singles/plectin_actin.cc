// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// PlectinActin type for Single was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-23)
/*
   This class is based on the Picket type and is used to model a single that is anchored to the bounding
   space. The anchoring point is obtained from projecting the PlectinActin position to the space boundary,
   and it moves affinely with it when the space boundary is deformed.
*/
#include "plectin_actin.h"
#include "simul.h"
#include "meca.h"
#include "modulo.h"


extern Modulo const* modulo;


PlectinActin::PlectinActin(SingleProp const* p, Vector const& w)
: Single(p, w)
{
#if ( 0 )
    if ( p->diffusion > 0 )
        throw InvalidParameter("single:diffusion cannot be > 0 if activity=fixed");
#endif
}


PlectinActin::~PlectinActin()
{
    //std::clog<<"~PlectinActin("<<this<<")"<<std::endl;
}


void PlectinActin::beforeDetachment(Hand const*)
{
    assert_true( attached() );

    SingleSet * set = static_cast<SingleSet*>(objset());
    if ( set )
        set->relinkD(this);
}


void PlectinActin::stepF(Simul& sim)
{
    assert_false( sHand->attached() );

    // translate single to be always confined on actin-rich space boundary
    Space const* space = simul().findSpace("cell");
    translate(space->projectPlectinActin(sPos) - sPos);

    sHand->stepUnattached(sim, sPos);
}


void PlectinActin::stepA()
{
    assert_true( sHand->attached() );

    // translate single to be always confined on actin-rich space boundary
    Space const* space = simul().findSpace("cell");
    translate(space->projectPlectinActin(sPos) - sPos);

    Vector f = force();
    sHand->stepLoaded(f, f.norm());

}


/**
 This calculates the force corresponding to addPointClamp()
 */
Vector PlectinActin::force() const
{
    assert_true( sHand->attached() );
    Vector d = sPos - posHand();
    
    if ( modulo )
        modulo->fold(d);
    
    return prop->stiffness * d;
}


void PlectinActin::setInteractions(Meca & meca) const
{
    assert_true( prop->length == 0 );

    // apply interaction to relevant point on meca
    meca.addPointClamp(sHand->interpolation(), sPos, prop->stiffness);
    //meca.addLineClamp(sHand->interpolation(), sPos, sHand->dirFiber(), prop->stiffness);
}


