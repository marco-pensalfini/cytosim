// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// Nematic organizer was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This organizer models a collection of fibers whose nematic director and order can be 
   controlled externally via a potential of prescribed stiffness. All fibers share the 
   same individual length and follow a simple turnover algorithm.
*/

#ifndef NEMATIC_PROP_H
#define NEMATIC_PROP_H

#include "real.h"
#include "property.h"
#include "common.h"

class FiberSet;


/// Property for Nematic
/**
 @ingroup Properties
*/
class NematicProp : public Property
{
    friend class Nematic;
    
public:
    
    /**
     @defgroup NematicPar Parameters of Nematic
     @ingroup Parameters
     @{
     */
 
    /// stiffness of the potential that controls the value of nematicS
    real          stiffness;

    /// nematic order parameter S, controlling fiber alignment
    real          nematicS;
    
    /// length of fibers that are part of this nematic organizer
    real          length;

    /// main direction of the nematic orientation: cos(alpha), sin(alpha)
    real          direction[2];

    /// rate for fiber turnover: we will detach hands & displace 1 fiber
    real          fiber_rate;
    

public:
 
    /// constructor
    NematicProp(const std::string& n) : Property(n)  { clear(); }
    
    /// destructor
    ~NematicProp() { }
    
    /// identifies the property
    std::string category() const { return "nematic"; }
    
    /// set default values
    void clear();
    
    /// set from a Glossary
    void read(Glossary&);
    
    /// check and derive parameters
    void complete(Simul const&);
    
    /// return a carbon copy of object
    Property* clone() const { return new NematicProp(*this); }

    /// write all values
    void write_values(std::ostream&) const;
    
};

#endif

