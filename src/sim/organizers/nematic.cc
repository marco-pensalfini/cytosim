// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// Nematic organizer was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This organizer models a collection of fibers whose nematic director and order can be 
   controlled externally via a potential of prescribed stiffness. All fibers share the 
   same individual length and follow a simple turnover algorithm.
*/

#include "dim.h"
#include "assert_macro.h"
#include "nematic.h"
#include "exceptions.h"
#include "mecapoint.h"
#include "interpolation.h"
#include "fiber_prop.h"
#include "glossary.h"
#include "simul.h"
#include "meca.h"
#include "interface.h"
#include <cmath>

void Nematic::step()
{
    Simul & sim = simul();

#if ( DIM == 2 )

    if ( nbOrganized() ) // need at least 1 item in organizer to apply turnover
    {
        ///distinguish whether we have multiple events per time step or 1 every some steps
        if ( simul().time_step() > 1.0/prop->fiber_rate )
        {
            ///how many events take place in this time step?
            unsigned Nevents = prop->fiber_rate*simul().time_step();
            ///pick Nevents random fibers, detach hands, and displace them
            for ( unsigned ii = 0; ii < Nevents; ++ii )
            {
                ///pick random fiber within organizer
                unsigned inx = RNG.pint32(nbOrganized());
                Fiber * fib = Fiber::toFiber(organized(inx));
                ///keep track of this fiber direction
                Vector2 dir = fib->avgDirection();
                ///detach hands so we don't drag them around
                fib->detachHands();
                ///get space dimensions and pick new random location
                Space * spc=sim.spaces.first();
                Vector inf, sup;
                spc->boundaries(inf,sup);
                Vector2 newPosMiddle = Vector2(RNG.sfloat()*sup[0],RNG.sfloat()*sup[1]);
                Vector2 newPosEndM   = newPosMiddle - 0.5*prop->length*dir;
                fib->setStraight(newPosEndM, dir);
            }
        }
        else
        {
            ///static variables to keep track of time at which event occurred
            static real time_lastEv = 0;
            static real last_rate = prop->fiber_rate;
            ///need to reset time_lastEv if the rate has changed!
            if ( last_rate != prop->fiber_rate )
            {
                last_rate = prop->fiber_rate;
                time_lastEv = simul().time();
            }
            ///check if event should take place
            if ( std::abs( (simul().time() - time_lastEv)*prop->fiber_rate - 1.0) < REAL_EPSILON )
            {
                ///pick random fiber within organizer
                unsigned inx = RNG.pint32(nbOrganized());
                Fiber * fib = Fiber::toFiber(organized(inx));
                ///keep track of this fiber direction
                Vector2 dir = fib->avgDirection();
                ///detach hands so we don't drag them around
                fib->detachHands();
                ///get space dimensions and pick new random location
                Space * spc=sim.spaces.first();
                Vector inf, sup;
                spc->boundaries(inf,sup);
                Vector2 newPosMiddle = Vector2(RNG.sfloat()*sup[0],RNG.sfloat()*sup[1]);
                Vector2 newPosEndM   = newPosMiddle - 0.5*prop->length*dir;
                fib->setStraight(newPosEndM, dir);
                ///update time of last event
                time_lastEv = simul().time();
            }
        }
    }

#endif

}

/**
 Control fiber nematic ordering by applying nodal forces corresponding to restraining energy
 */
void Nematic::setInteractions(Meca & meca) const
{
    assert_true( linked() );

#if ( DIM == 2 )

    /// 0. Set some nematic parameter values
    nemS     = prop->nematicS;
    nemStiff = prop->stiffness;
    nemDir   = prop->direction;

    /// 1. Determine target Q value based on nemDir and nemS
    Vector2 lamda, mu;

    lamda = nemDir/nemDir.norm();
    mu[0] = lamda[1];
    mu[1] = -lamda[0];
    Q_target = 0.5 * nemS * ( Matrix22::outerProduct(lamda,lamda) - Matrix22::outerProduct(mu,mu) );

    /// 2. Determine current Q value based on all fiber segments in the organizer
    Matrix22 Id = Matrix22::identity();
    Q_curr.reset();
    numSeg=0;
    for ( unsigned ii = 0; ii < nbOrganized(); ++ii )
    {
        Fiber * fib = Fiber::toFiber(organized(ii));
        for ( unsigned jj = 0; jj < fib->nbSegments(); ++jj )
        {
            Vector2 thisSeg = (fib->posPoint(jj+1) - fib->posPoint(jj)) / fib->segmentation();
            Q_curr += Matrix22::outerProduct(thisSeg, thisSeg) - 0.5 * Id;
        }
        numSeg += fib->nbSegments();
    }
    Q_curr = Q_curr * (1.0/numSeg);

    /// 3. Determine value of energy penalizing deviations of Q_curr from Q_target
    nemEnergy  = 0.5 * nemStiff * (Q_curr - Q_target).line(0)[0] * (Q_curr - Q_target).line(0)[0];
    nemEnergy += 0.5 * nemStiff * (Q_curr - Q_target).line(0)[1] * (Q_curr - Q_target).line(0)[1];
    nemEnergy += 0.5 * nemStiff * (Q_curr - Q_target).line(1)[0] * (Q_curr - Q_target).line(1)[0];
    nemEnergy += 0.5 * nemStiff * (Q_curr - Q_target).line(1)[1] * (Q_curr - Q_target).line(1)[1];

    /// 4. Apply forces to reduce system's penalty energy
    Vector2 F;
    Matrix22 dQ_dx_x, dQ_dx_y;
    Matrix22 dEn_dQ = nemStiff * (Q_curr - Q_target);
    nemStress  = dEn_dQ;

    for ( unsigned ii = 0; ii < nbOrganized(); ++ii )
    {
        Fiber * fib = Fiber::toFiber(organized(ii));

        for ( unsigned jj = 0; jj < fib->nbPoints(); ++jj )
        {
            // compute dQ_dx_ in energy derivative
            F.reset();
            dQ_dx_x.reset();
            dQ_dx_y.reset();
            // NOTE: first point has contributions only from nextSeg
            //       last point only from thisSeg, others from both
            if ( jj == 0 )
            {
                Vector2 nextSeg = (fib->posPoint(jj+1) - fib->posPoint(jj)) / fib->segmentation();
                Vector2 DnextSeg_DthisPtX = Vector2( -pow(fib->posPoint(jj+1)[1] - fib->posPoint(jj)[1], 2),  (fib->posPoint(jj+1)[0] - fib->posPoint(jj)[0]) * (fib->posPoint(jj+1)[1] - fib->posPoint(jj)[1]) ) / pow(fib->segmentation(),3);
                Vector2 DnextSeg_DthisPtY = Vector2(  (fib->posPoint(jj+1)[0] - fib->posPoint(jj)[0]) * (fib->posPoint(jj+1)[1] - fib->posPoint(jj)[1]), -pow(fib->posPoint(jj+1)[0] - fib->posPoint(jj)[0], 2) ) / pow(fib->segmentation(),3);
                // x component
                dQ_dx_x += Matrix22::outerProduct(DnextSeg_DthisPtX, nextSeg);
                dQ_dx_x += Matrix22::outerProduct(nextSeg, DnextSeg_DthisPtX);
                // y component
                dQ_dx_y += Matrix22::outerProduct(DnextSeg_DthisPtY, nextSeg);
                dQ_dx_y += Matrix22::outerProduct(nextSeg, DnextSeg_DthisPtY);
            }
            else if ( jj == fib->nbPoints()-1 )
            {
                Vector2 thisSeg = (fib->posPoint(jj) - fib->posPoint(jj-1)) / fib->segmentation();
                //
                Vector2 DthisSeg_DthisPtX = Vector2(  pow(fib->posPoint(jj)[1] - fib->posPoint(jj-1)[1], 2), - (fib->posPoint(jj)[0] - fib->posPoint(jj-1)[0]) * (fib->posPoint(jj)[1] - fib->posPoint(jj-1)[1]) ) / pow(fib->segmentation(),3);
                Vector2 DthisSeg_DthisPtY = Vector2( -(fib->posPoint(jj)[0] - fib->posPoint(jj-1)[0]) * (fib->posPoint(jj)[1] - fib->posPoint(jj-1)[1]),  pow(fib->posPoint(jj)[0] - fib->posPoint(jj-1)[0], 2) ) / pow(fib->segmentation(),3);
                // x component
                dQ_dx_x += Matrix22::outerProduct(DthisSeg_DthisPtX, thisSeg);
                dQ_dx_x += Matrix22::outerProduct(thisSeg, DthisSeg_DthisPtX);
                // y component
                dQ_dx_y += Matrix22::outerProduct(DthisSeg_DthisPtY, thisSeg);
                dQ_dx_y += Matrix22::outerProduct(thisSeg, DthisSeg_DthisPtY);
            }
            else
            {
                Vector2 thisSeg = (fib->posPoint(jj) - fib->posPoint(jj-1)) / fib->segmentation();
                Vector2 nextSeg = (fib->posPoint(jj+1) - fib->posPoint(jj)) / fib->segmentation();
                //
                Vector2 DthisSeg_DthisPtX = Vector2(  pow(fib->posPoint(jj)[1] - fib->posPoint(jj-1)[1], 2), - (fib->posPoint(jj)[0] - fib->posPoint(jj-1)[0]) * (fib->posPoint(jj)[1] - fib->posPoint(jj-1)[1]) ) / pow(fib->segmentation(),3);
                Vector2 DthisSeg_DthisPtY = Vector2( -(fib->posPoint(jj)[0] - fib->posPoint(jj-1)[0]) * (fib->posPoint(jj)[1] - fib->posPoint(jj-1)[1]),  pow(fib->posPoint(jj)[0] - fib->posPoint(jj-1)[0], 2) ) / pow(fib->segmentation(),3);
                //
                Vector2 DnextSeg_DthisPtX = Vector2( -pow(fib->posPoint(jj+1)[1] - fib->posPoint(jj)[1], 2),  (fib->posPoint(jj+1)[0] - fib->posPoint(jj)[0]) * (fib->posPoint(jj+1)[1] - fib->posPoint(jj)[1]) ) / pow(fib->segmentation(),3);
                Vector2 DnextSeg_DthisPtY = Vector2(  (fib->posPoint(jj+1)[0] - fib->posPoint(jj)[0]) * (fib->posPoint(jj+1)[1] - fib->posPoint(jj)[1]), -pow(fib->posPoint(jj+1)[0] - fib->posPoint(jj)[0], 2) ) / pow(fib->segmentation(),3);
                // x component
                dQ_dx_x += Matrix22::outerProduct(DthisSeg_DthisPtX, thisSeg);
                dQ_dx_x += Matrix22::outerProduct(thisSeg, DthisSeg_DthisPtX);
                dQ_dx_x += Matrix22::outerProduct(DnextSeg_DthisPtX, nextSeg);
                dQ_dx_x += Matrix22::outerProduct(nextSeg, DnextSeg_DthisPtX);
                // y component
                dQ_dx_y += Matrix22::outerProduct(DthisSeg_DthisPtY, thisSeg);
                dQ_dx_y += Matrix22::outerProduct(thisSeg, DthisSeg_DthisPtY);
                dQ_dx_y += Matrix22::outerProduct(DnextSeg_DthisPtY, nextSeg);
                dQ_dx_y += Matrix22::outerProduct(nextSeg, DnextSeg_DthisPtY);
            }
            dQ_dx_x *= 1.0/numSeg;
            dQ_dx_y *= 1.0/numSeg;

            // compute x force component
            F[0] -= dEn_dQ.line(0)[0] * dQ_dx_x.line(0)[0];
            F[0] -= dEn_dQ.line(0)[1] * dQ_dx_x.line(0)[1];
            F[0] -= dEn_dQ.line(1)[0] * dQ_dx_x.line(1)[0];
            F[0] -= dEn_dQ.line(1)[1] * dQ_dx_x.line(1)[1];
            // compute y force component
            F[1] -= dEn_dQ.line(0)[0] * dQ_dx_y.line(0)[0];
            F[1] -= dEn_dQ.line(0)[1] * dQ_dx_y.line(0)[1];
            F[1] -= dEn_dQ.line(1)[0] * dQ_dx_y.line(1)[0];
            F[1] -= dEn_dQ.line(1)[1] * dQ_dx_y.line(1)[1];

            // apply force to mecapoint to induce alignment
            meca.addForce(Mecapoint(fib, jj  ), F);

        }

    }

#endif

}

//------------------------------------------------------------------------------

ObjectList Nematic::build(Glossary& opt, Simul& sim)
{
    assert_true(prop);
    ObjectList res;

#if ( DIM == 2 )

    Space * spc=sim.spaces.first(); ///find space
    
    unsigned cnt = 0;
    std::string type, spec;
    opt.set(cnt,  "fibers");
    opt.set(type, "fibers", 1);
    opt.set(spec, "fibers", 2);
    
    if ( cnt <= 0 )
        throw InvalidParameter("number of fibers (fibers[0]) must be specified and >= 1");
    
    nbOrganized(cnt);
    
    for ( unsigned inx = 0; inx < cnt; ++inx )
    {
        /// make new fiber
        Glossary fiber_opt(spec);
        ObjectList objs = sim.fibers.newObjects(type, fiber_opt);

        /// if object (fiber) was created correctly, adjust length and add to organizer
        if ( objs.size() > 0 )
        {
            /// get fiber
            Fiber * fib = Fiber::toFiber(objs[0]);

            if ( fib ) /// ensure it's a fiber...
            {
                /// place fiber horizontally, with its center on the origin, and with the correct length
                fib->setStraight(Vector2(-0.5*prop->length,0), Vector2(1,0), prop->length);

                /// rotate to random direction withouth changing length
                Vector2 dir;
                real a = 2.0*M_PI*RNG.preal();
                dir[0] = cos(a);
                dir[1] = sin(a);

                fib->setStraight(-0.5*prop->length*dir, dir);

                /// random placement within space boundaries
                Isometry iso = spc->randomPlace();
                ObjectSet::moveObjects(objs, iso);

                /// insert fiber fib in organizer as inx-th item
                grasp(fib, inx);
            }

            /// append objs to list of objects
            res.append(objs);
        }
    }

#endif
    
    return res;
}

Nematic::~Nematic()
{
    prop = nullptr;
}


